// Variáveis globais
var username; // O nome do usuário
var socket; // O socket para a comunicação com o servidor
var current_page; // A página atual do site
var current_group; // O grupo atual selecionado
var current_community; // A comunidade atual selecionada
var current_ad; // O anúncio ou venda atual selecionado

// Função que é executada quando a página é carregada
function init() {
    // Solicita o nome do usuário
    username = prompt("Digite seu nome para entrar no chat");
    // Cria um socket para se conectar ao servidor
    socket = new WebSocket("ws://localhost:8080");
    // Define as funções que serão chamadas quando o socket receber ou enviar mensagens
    socket.onopen = onOpen;
    socket.onmessage = onMessage;
    socket.onclose = onClose;
    socket.onerror = onError;
    // Mostra a página inicial do site
    showPage("chat");
}

// Função que é chamada quando o socket se conecta ao servidor
function onOpen(event) {
    // Envia uma mensagem ao servidor informando o nome do usuário
    socket.send(JSON.stringify({type: "join", username: username}));
}

// Função que é chamada quando o socket recebe uma mensagem do servidor
function onMessage(event) {
    // Converte a mensagem em um objeto JSON
    var message = JSON.parse(event.data);
    // Verifica o tipo da mensagem
    switch (message.type) {
        case "chat": // Mensagem do chat geral
            // Adiciona a mensagem na div de mensagens do chat geral
            var messages = document.getElementById("messages");
            var p = document.createElement("p");
            p.textContent = message.username + ": " + message.text;
            messages.appendChild(p);
            // Faz a div de mensagens rolar para baixo
            messages.scrollTop = messages.scrollHeight;
            break;
        case "group": // Mensagem de um grupo
            // Verifica se o grupo da mensagem é o mesmo que o grupo atual
            if (message.group == current_group) {
                // Adiciona a mensagem na div de mensagens do grupo
                var group_chat = document.getElementById("group-chat");
                var p = document.createElement("p");
                p.textContent = message.username + ": " + message.text;
                group_chat.appendChild(p);
                // Faz a div de mensagens rolar para baixo
                group_chat.scrollTop = group_chat.scrollHeight;
            }
            break;
        case "community": // Mensagem de uma comunidade
            // Verifica se a comunidade da mensagem é a mesma que a comunidade atual
            if (message.community == current_community) {
                // Adiciona a mensagem na div de mensagens da comunidade
                var community_chat = document.getElementById("community-chat");
                var p = document.createElement("p");
                p.textContent = message.username + ": " + message.text;
                community_chat.appendChild(p);
                // Faz a div de mensagens rolar para baixo
                community_chat.scrollTop = community_chat.scrollHeight;
            }
            break;
        case "ad": // Mensagem de um anúncio ou venda
            // Verifica se o anúncio ou venda da mensagem é o mesmo que o anúncio ou venda atual
            if (message.ad == current_ad) {
                // Adiciona a mensagem na div de mensagens do anúncio ou venda
                var ad_chat = document.getElementById("ad-chat");
                var p = document.createElement("p");
                p.textContent = message.username + ": " + message.text;
                ad_chat.appendChild(p);
                // Faz a div de mensagens rolar para baixo
                ad_chat.scrollTop = ad_chat.scrollHeight;
            }
            break;
        case "groups": // Lista de grupos disponíveis
            // Limpa a div de grupos
            var groups_list = document.getElementById("groups-list");
            groups_list.innerHTML = "";
            // Adiciona cada grupo na div de grupos
            for (var i = 0; i < message.groups.length; i++) {
                var group = message.groups[i];
                var li = document.createElement("li");
                var a = document.createElement("a");
                a.href = "#";
                a.textContent = group;
                a.onclick = function() {
                    selectGroup(this.textContent);
                };
                li.appendChild(a);
                groups_list.appendChild(li);
            }
            break;
        case "communities": // Lista de comunidades disponíveis
            // Limpa a div de comunidades
            var communities_list = document.getElementById("communities-list");
            communities_list.innerHTML = "";
            // Adiciona cada comunidade na div de comunidades
            for (var i = 0; i < message.communities.length; i++) {
                var community = message.communities[i];
                var li = document.createElement("li");
                var a = document.createElement("a");
                a.href = "#";
                a.textContent = community;
                a.onclick = function() {
                    selectCommunity(this.textContent);
                };
                li.appendChild(a);
                communities_list.appendChild(li);
            }
            break;
        case "ads": // Lista de anúncios e vendas disponíveis
            // Limpa a div de anúncios e vendas
            var ads_list = document.getElementById("ads-list");
            ads_list.innerHTML = "";
            // Adiciona cada anúncio ou venda na div de anúncios e vendas
            for (var i = 0; i < message.ads.length; i++) {
                var ad = message.ads[i];
                var li = document.createElement("li");
                var a = document.createElement("a");
                a.href = "#";
                a.textContent = ad;
                a.onclick = function() {
                    selectAd(this.textContent);
                };
                li.appendChild(a);
                ads_list.appendChild(li);
            }
            break;
    }
}

// Função que é chamada quando o socket se desconecta do servidor
function onClose(event) {
    // Mostra uma mensagem de alerta
    alert("Você foi desconectado do chat");
}
